import unittest

from flatics.apps import app, db
from flatics.apps.model import User
from flatics.tests.base import BaseTestCase

class TestApis(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_index(self):
        response = self.app.get('/')
        assert response.status_code == 200
        assert response.data == b'Hello World!'

class TestUser(BaseTestCase): 
    def test_user(self):
        user = User('user','test@test.com', 'secret')
        db.session.add(user)
        db.session.commit()
        assert user == User.query.filter_by(username='user').first()
