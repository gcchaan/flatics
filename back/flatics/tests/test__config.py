import unittest

from flask import current_app
from flask_testing import TestCase

import os

from flatics.apps import app

class TestDevelopmentConfig(TestCase):
    def create_app(self):
        app.config.from_object('flatics.apps.config.DevelopmentConfig')
        return app

    def test_app_is_development(self):
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertFalse(current_app is None)
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] == os.getenv(
                'FLATICS_DB_URI',
                'postgresql://postgres:@localhost/flatics')
        )


class TestTestingConfig(TestCase):
    def create_app(self):
        app.config.from_object('flatics.apps.config.TestingConfig')
        return app

    def test_app_is_testing(self):
        self.assertTrue(app.config['DEBUG'])
        self.assertTrue(
            app.config['SQLALCHEMY_DATABASE_URI'] == os.getenv(
                'FLATICS_TEST_DB_URI',
                'postgresql://postgres:@localhost/flatics_test')
        )
