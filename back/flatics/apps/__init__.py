import os
from flask import Flask
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object(
    'flatics.apps.config.' +
    os.getenv('FLATICS_SETTING', 'Development') + 'Config')
bcrypt = Bcrypt(app)
db = SQLAlchemy(app)

from flatics.apps import model
from flatics.apps.view import api
from flatics.apps.view.auth import auth_blueprint
app.register_blueprint(auth_blueprint)
