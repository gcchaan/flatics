import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.getenv('FLATICS_SECRET_KEY', 'my_precious')
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'FLATICS_DB_URI',
        'postgresql://postgres:@localhost/flatics')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'FLATICS_TEST_DB_URI',
        'postgresql://postgres:@localhost/flatics_test')
