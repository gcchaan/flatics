from flask import jsonify
from flatics.apps import app, model

@app.route('/')
def hello_world():
    return "Hello World!"

@app.route('/api')
def api_index():
    return "Hello API!"

@app.route('/api/user/<int:id>')
def user(id):
    user = model.User.query.get(id)
    responseObject = {
        'status': 'success!',
        'data': {
            'id': user.id,
            'name': user.username,
            'email': user.email,
        }
    }
    return jsonify(responseObject), 200
