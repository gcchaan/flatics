import os

def numCpus():
    if not hasattr(os, 'sysconf'):
        raise RuntimeError('No sysconf detected.')
    return os.sysconf('SC_NPROCESSORS_ONLN')

bind = 'unix:/back/tmp/socket/gunicorn.sock'
workers = numCpus() * 2 + 1

errorlog = 'tmp/logs/error.log'
loglevel = 'info'
accesslog = 'tmp/logs/access.log'
access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
