[dokcer]
required
- docker

build:
    $ docker-compose build upstream

run flask:
    $ docker-compose run -p 5000:5000 upstream python -m flask run --host=0.0.0.0

run flask with gunicorn:
    $ docker-compose run -p 8000:8000 upstream gunicorn -w 1 -b :8000 flatics:app

test:
    $ docker-compose run upstream python manage.py cov

[virtualenv]
requred
- Python 3.5+
- pip
  - virtualenv

$ virtualenv venv
$ source back/venv/bin/activate
(venv) $ export FLASK_APP=flatics
(venv) $ export FLASK_DEBUG=true
(venv) $ export FLATICS_SETTING=Development
(venv) $ export FLATICS_SECRET_KEY="{{ os.urandom(24) }}"

(venv) $ pip install -r requirements.txt

(venv) $ python manage.py runserver
or
(venv) $ gunicorn -w 1 -b :8000 flatics:app

(venv) $ python manage.py cov

log files:
    - /back/tmp/logs/error.log
    - /back/tmp/logs/access.log

[query sample]
$ psql -h 127.0.0.1 -U postgres
postgres=# CREATE DATABASE flatics;
postgres=# CREATE DATABASE flatics_test;
postgres=# \q
(venv) $ python manage.py create_db
(venv) $ python manage.py db migrate
(venv) $ python manage.py db init

(venv) $ python manage.py shell
>>> from flatics.apps.model import User
>>> admin = User('admin', 'admin@example.com', 'pass')
>>> guest = User('guest', 'guest@example.com', 'pass')

>>> from flatics.apps import db
>>> db.session.add(admin)
>>> db.session.add(guest)
>>> db.session.commit()

>>> User.query.all()
[<User 'admin'>, <User 'guest'>]
>>> User.query.filter_by(username='admin').first()
<User 'admin'>

[auth]
$ curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"username":"suzuki","email":"email@email.com","password":"123"}'  http://localhost/api/auth/register

$ curl -v -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"email":"email@email.com","password":"123"}'  http://localhost/api/auth/login

$ export AUTH_TOKEN=token
$ curl -H "Authorization: token $AUTH_TOKEN" http://localhost/api/auth/status

$ curl -v -H "Authorization: Bearer $AUTH_TOKEN" -X POST http://localhost/api/auth/logout
