.. image:: https://circleci.com/bb/gcchaan/flatics.svg?style=shield&circle-token=5d27023abd38583413fa7e30214034f0dcdfe482
  :target: https://circleci.com/bb/gcchaan/flatics

.. image:: https://codecov.io/bb/gcchaan/flatics/branch/master/graph/badge.svg?token=mT80c3HZA2
  :target: https://codecov.io/bb/gcchaan/flatics

you can develop with docker or own machine.


[dokcer]
required
- docker

build:
    $ docker-compose build

build & start with dettach:
    $ docker-compose up -d

stop & remove:
    $ docker-compose down

show logs:
    $ docker-compose logs

PostgreSQL
    $ psql -h 127.0.0.1 -U postgres